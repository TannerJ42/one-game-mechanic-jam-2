﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEditorTileController : MonoBehaviour
{
    public TileDef def;
    public SpriteRenderer spriteRenderer;

    public Sprite[] textures;

    void Awake()
    {
        Draw();
    }

    public TileDef getDef()
    {
        return def;
    }

    private void Draw()
    {
        if ((int)def.type == 0)
        {
            return;
        }

        Sprite texture = textures[(int)def.type - 1];
        spriteRenderer.sprite = texture;
    }

    public void SetDef(ref TileDef def)
    {
        this.def = def;
    }

    public void OnMouseDown()
    {
        if (!Input.GetKey(KeyCode.Space))
        {
            Increment();
        }
        else
        {
            Decrement();
        }
    }

    void Increment()
    {
        switch (def.type)
        {
            case TileType.One:
                def.type = TileType.Two;
                break;
            case TileType.Two:
                def.type = TileType.Three;
                break;
            case TileType.Three:
                def.type = TileType.Four;
                break;
            case TileType.Four:
                def.type = TileType.Five;
                break;
            case TileType.Five:
                def.type = TileType.One;
                break;
        }

        Draw();
    }

    void Decrement()
    {
        switch (def.type)
        {
            case TileType.One:
                def.type = TileType.Five;
                break;
            case TileType.Two:
                def.type = TileType.One;
                break;
            case TileType.Three:
                def.type = TileType.Two;
                break;
            case TileType.Four:
                def.type = TileType.Three;
                break;
            case TileType.Five:
                def.type = TileType.Four;
                break;
        }

        Draw();
    }

   void Update()
    {

    }
}
