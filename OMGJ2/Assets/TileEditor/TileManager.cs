﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TileManager : MonoBehaviour {

    public Transform tileParent;

    public GameObject prefab;

    private LevelDef def;

    public void setDef(LevelDef def)
    {
        this.def = def;
        PlaceTiles();
    }

    public LevelDef getDef()
    {
        return this.def;
    }

    private void PlaceTiles()
    {
        foreach (Transform child in tileParent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        for (int i = 0; i < def.tiles.Count; i++)
        {
            TileDef tileDef = def.tiles[i];
            GameObject obj = Instantiate(prefab, new Vector3(0.6f * tileDef.x, 0.6f * tileDef.y, 0), Quaternion.identity, this.tileParent);
            LevelEditorTileController cont = obj.GetComponent<LevelEditorTileController>();
            if (cont != null)
            {
                cont.SetDef(ref tileDef);
            }
        }
    }

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}
}
