﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultScreenController : MonoBehaviour {
    public TextMesh result;
    public TextMesh highScore;
    public TextMesh goalScore;
    public TextMesh scoreEarned;
    public TextMesh blocksLeft;

	// Use this for initialization
	void Start () {
        result.text = GameplaySettings.WonRound ? "SUCCESS" : "TRY AGAIN";
        scoreEarned.text = "SCORE: " + GameplaySettings.EarnedPoints;
        goalScore.text = "GOAL: " + GameplaySettings.PassingScore;
        highScore.text = "HIGHEST SCORE: " + PlayerPrefs.GetInt(GameplaySettings.Level.name);
        blocksLeft.text = "BLOCKS REMAIN: " + GameplaySettings.BlocksLeft;

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
