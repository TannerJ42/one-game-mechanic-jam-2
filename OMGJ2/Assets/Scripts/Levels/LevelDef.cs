﻿using System;
using System.Collections.Generic;

[Serializable]
public class LevelDef
{
    public string name;
    public Theme theme;

    public int width;
    public int height;

    public List<TileDef> tiles;
    public List<RoundDef> rounds;

}