﻿using System;

[Serializable]
public enum Theme
{
    None,
    RobinHood,
    KingArthur,
    WizardOfOz,
    AliceInWonderland,
    RedRidingHood
}
