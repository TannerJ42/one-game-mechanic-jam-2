﻿using System;

[Serializable]
public class RoundDef
{
    public TileType[] order;
    public int taps;
}
