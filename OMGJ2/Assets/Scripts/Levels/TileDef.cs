﻿using System;

[Serializable]
public class TileDef
{
    public int x;
    public int y;

    public TileType type;
}