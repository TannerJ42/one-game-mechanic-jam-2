﻿public enum Scene
{
    TitleScreen = 0,
    MainMenu = 1,
    WorldSelect = 2,
    World0 = 11,
    World1 = 3,
    World2 = 4,
    World3 = 5,
    World4 = 6,
    World5 = 7,
    Gameplay = 8,
    Result = 9,
    Credits = 10
}
