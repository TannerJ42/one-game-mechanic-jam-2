﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileController : MonoBehaviour {

    public SpriteRenderer spriteRenderer;
    private TileDef tileDef;

    public TileDef TileDef {
        get
        {
            return tileDef;
        }
        set
        {
            tileDef = value;
            Load();
        }
    }

    public GameplayController Controller { get; set; }
    public bool Matched = false;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseDown()
    {
        Increment();
        Controller.OnTileTapped();
    }

    void Increment()
    {
        TileType newType = Controller.GetNext(TileDef.type);
        if (newType != TileDef.type && newType != TileType.None)
        {
            SFXController.Instance.PlayClick();
            TileDef.type = newType;
            Load();
        }

    }

    void Load()
    {
        if (tileDef.type != TileType.None)
        {
            Sprite texture = Controller.GetSprite(tileDef.type);
            spriteRenderer.sprite = texture;
        }

    }
}
