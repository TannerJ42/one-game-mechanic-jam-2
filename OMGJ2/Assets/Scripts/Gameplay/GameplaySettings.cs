﻿
using UnityEngine;

public class GameplaySettings
{
    public static TextAsset Level;
    public static RoundDef[] Rounds;
    public static int PassingScore;
    public static Theme Theme;
    public static bool WonRound;
    public static int EarnedPoints;
    public static int BlocksLeft;
    public static Sprite[] Sprites;
    public static Sprite background;
    public static Scene LastWorldScene;
}
