﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayController : MonoBehaviour {
    public TextAsset levelToLoad;
    public LevelDef currentLevel;
    public GameObject prefab;
    public Transform tileParent;
    public Sprite[] defaults;

    public TextMesh TapsText;
    public TextMesh RoundsText;
    public TextMesh ScoreText;
    public TextMesh RequiredText;

    public KeyUIController keyUIController;

    public int pointsPerTile = 100;
    public int pointsPerTile1 = 150;

    private Dictionary<TileType, Sprite> sprites;

    private int pointsEarned = 0;
    private int roundsLeft;
    public int maxRounds
    {
        get { return roundDefs.Length; }
    }

    private int remainingTaps;

    public RoundDef[] roundDefs;

    private TileController[][] tileArray;

    // Use this for initialization
    void Start () {

        if (GameplaySettings.Level == null)
        {
            GameplaySettings.Level = levelToLoad;
        }
        currentLevel = JsonUtility.FromJson<LevelDef>(GameplaySettings.Level.text);

        if (GameplaySettings.Sprites == null)
        {
            GameplaySettings.Sprites = defaults;
        }

        roundDefs = GameplaySettings.Rounds ?? roundDefs;

        LoadTextures();
        LoadLevel();
        roundsLeft = maxRounds;
        remainingTaps = roundDefs[maxRounds - roundsLeft].taps;

        if (GameplaySettings.background != null)
        {
            GameObject background = new GameObject();
            SpriteRenderer rend = background.AddComponent<SpriteRenderer>();
            rend.sprite = GameplaySettings.background;
            background.transform.position = new Vector3(0, 0, 10);
        }

        DrawUI();
    }

    void LoadTextures()
    {
        sprites = new Dictionary<TileType, Sprite>();
        for (int i = 0; i < GameplaySettings.Sprites.Length; i++)
        {
            sprites.Add((TileType)(i + 1), GameplaySettings.Sprites[i]);
        }
    }

	// Update is called once per frame
	void Update ()
    {
		
	}

    public bool OnTileTapped()
    {
        if (roundsLeft == 0)
        {
            return false;
        }
        remainingTaps--;

        if (remainingTaps == 0)
        {
            OnRoundEnded();
        }
        DrawUI();
        return true;
    }

    public void DrawUI()
    {
        this.TapsText.text = "TAPS LEFT: " + remainingTaps;
        this.RoundsText.text = "ROUNDS LEFT: " + roundsLeft;
        this.ScoreText.text = "SCORE: " + pointsEarned;
        this.RequiredText.text = "SCORE GOAL: " + GameplaySettings.PassingScore;
        if (roundsLeft > 0){
            this.keyUIController.SetKey(roundDefs[roundDefs.Length - roundsLeft].order, GameplaySettings.Sprites);
        }

    }

    private int CountBlocks()
    {
        int score = 0;
        for (int x = 0; x < currentLevel.width; x++)
        {
            for (int y = 0; y < currentLevel.height; y++)
            {
                if (GetTileAt(x, y) != null)
                {
                    score++;
                }
            }
        }
        return score;
    }

    public void OnRoundEnded()
    {
        roundsLeft -= 1;

        if (roundsLeft > 0)
        {
            remainingTaps = roundDefs[maxRounds - roundsLeft].taps;
        }


        CheckForMatches();

        MarkForDeletion();

        DropDown();

        if (roundsLeft == 0 || CountBlocks() == 0)
        {
            Invoke("OnGameOver", 2f);
        }
    }

    public void CheckForMatches()
    {
        for (int x = 0; x < currentLevel.width; x++)
        {
            for (int y = 0; y < currentLevel.height; y++)
            {
                TileController tileController = GetTileAt(x, y);
                if (tileController == null)
                {
                    continue;
                }
                TileController above = GetTileAt(x, y - 1);
                TileController below = GetTileAt(x, y + 1);
                TileController right = GetTileAt(x + 1, y);
                TileController left = GetTileAt(x - 1, y);

                int count = 0;
                count += above != null && tileController.TileDef.type == above.TileDef.type ? 1 : 0;
                count += below != null && tileController.TileDef.type == below.TileDef.type ? 1 : 0;
                count += right != null && tileController.TileDef.type == right.TileDef.type ? 1 : 0;
                count += left != null && tileController.TileDef.type == left.TileDef.type ? 1 : 0;

                if (count > 1)
                {
                    tileController.Matched = true;
                    MatchNeighbors(tileController);
                }
            }
        }
    }

    public void MatchNeighbors(TileController tile)
    {
        TileController above = GetTileAt(tile.TileDef.x, tile.TileDef.y - 1);
        TileController below = GetTileAt(tile.TileDef.x, tile.TileDef.y + 1);
        TileController right = GetTileAt(tile.TileDef.x + 1, tile.TileDef.y);
        TileController left = GetTileAt(tile.TileDef.x - 1, tile.TileDef.y);

        if (above != null && above.TileDef.type == tile.TileDef.type)
        {
            above.Matched = true;
        }

        if (right != null && right.TileDef.type == tile.TileDef.type)
        {
            right.Matched = true;
        }

        if (left != null && left.TileDef.type == tile.TileDef.type)
        {
            left.Matched = true;
            MatchNeighbors(left);
        }

        if (below != null && below.TileDef.type == tile.TileDef.type)
        {
            below.Matched = true;
            MatchNeighbors(below);
        }
    }

    public void MarkForDeletion()
    {
        for (int x = 0; x < currentLevel.width; x++)
        {
            for (int y = 0; y < currentLevel.height; y++)
            {
                TileController tileController = GetTileAt(x, y);
                if (tileController != null && tileController.Matched)
                {
                    Destroy(tileController.gameObject);
                    RemoveTileAt(x, y);
                    if (tileController.TileDef.type == roundDefs[0].order[0])
                    {
                        pointsEarned += pointsPerTile1;
                    }
                    else
                    {
                        pointsEarned += pointsPerTile;
                    }
                }
            }
        }
    }

    public void DropDown()
    {
        for (int x = 0; x < currentLevel.width; x++)
        {
            int numberToDrop = 0;
            for (int y = 0; y < currentLevel.height; y++)
            {
                TileController tileController = GetTileAt(x, y);
                if (tileController == null)
                {
                    numberToDrop++;
                }
                else
                {
                    if (y > 0 && numberToDrop > 0)
                    {
                        tileArray[x][y] = null;
                        tileController.TileDef.y = tileController.TileDef.y - numberToDrop;
                        tileArray[x][tileController.TileDef.y] = tileController;

                        Vector3 pos = tileController.transform.position;
                        pos.y -= 0.6f * numberToDrop;
                        tileController.transform.position = pos;

                    }
                }
            }
        }
    }

    private TileController GetTileAt(int x, int y)
    {
        if (x < 0 || x > currentLevel.width - 1 || y < 0 || y > currentLevel.height - 1)
        {
            return null;
        }

        return tileArray[x][y];
    }

    private void RemoveTileAt(int x, int y)
    {
        tileArray[x][y] = null;
    }

    public void OnGameOver()
    {
        int blockCount = CountBlocks();
        GameplaySettings.BlocksLeft = blockCount;
        GameplaySettings.WonRound = pointsEarned >= GameplaySettings.PassingScore ? true : false;
        GameplaySettings.EarnedPoints = pointsEarned;

        // save won
        if (pointsEarned >= GameplaySettings.PassingScore)
        {
            string wonkey = GameplaySettings.Level.name + "_won";
            PlayerPrefs.SetInt(wonkey, 1);

            if (blockCount == 0)
            {
                string masteredkey = GameplaySettings.Level.name + "_mastered";
                PlayerPrefs.SetInt(masteredkey, 1);
            }
        }

        // save as high score
        int highScore = pointsEarned;
        if (PlayerPrefs.HasKey(GameplaySettings.Level.name))
        {
            int prevHigh = PlayerPrefs.GetInt(GameplaySettings.Level.name);
            if (prevHigh > highScore)
            {
                highScore = prevHigh;
            }
        }

        PlayerPrefs.SetInt(currentLevel.name, highScore);

        SceneManager.LoadScene((int)Scene.Result);
    }

    public Sprite GetSprite(TileType type)
    {
        return sprites[type];
    }

    public TileType GetNext(TileType type)
    {
        if (roundsLeft != 0)
        {
            TileType[] tileOrder = roundDefs[roundDefs.Length - roundsLeft].order;
            for (int i = 0; i < tileOrder.Length - 1; i++)
            {
                if (tileOrder[i] == type)
                {
                    return tileOrder[i + 1];
                }
            }
            return tileOrder[0];

        }
        return TileType.None;
    }

    private void LoadLevel()
    {
        tileArray = new TileController[currentLevel.width][];
        for (int i = 0; i < currentLevel.width; i++)
        {
            tileArray[i] = new TileController[currentLevel.height];
        }
        List<TileDef> tiles = currentLevel.tiles;

        for (int i = 0; i < tiles.Count; i++)
        {
            TileDef tileDef = tiles[i];
            GameObject obj = Instantiate(prefab);
            Vector3 newPos = new Vector3(0.6f * (tileDef.x - currentLevel.width / 2), 0.6f * tileDef.y, 0);
            obj.transform.parent = tileParent;
            newPos += tileParent.position;
            obj.transform.position = newPos;
            TileController cont = obj.GetComponent<TileController>();
            if (cont != null)
            {
                cont.Controller = this;
                cont.TileDef = tileDef;
            }
            tileArray[tileDef.x][tileDef.y] = cont;
        }

        Vector3 parentPosition = new Vector3(0.31f, -4.46f, 0);

        tileParent.transform.position = parentPosition;
    }
}
