﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransitionOnPress : MonoBehaviour
{
    public GameObject normal;
    public GameObject pressed;

    public Scene scene;

    enum State
    {
        Normal,
        Pressed
    }

    private State state;

    // Use this for initialization
    void Start()
    {
        normal.SetActive(true);
        pressed.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case State.Pressed:
                if (Input.GetMouseButtonUp(0))
                {
                    Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero);
                    if (hit && hit.collider != null)
                    {
                        if (hit.collider.gameObject == this.gameObject)
                        {
                            OnClick();
                        }
                    }

                    OnRelease();
                }
                break;
        }
    }

    void OnMouseDown()
    {
        OnPress();
    }

    void OnPress()
    {
        if (state != State.Pressed)
        {
            normal.SetActive(false);
            pressed.SetActive(true);
            state = State.Pressed;
            SFXController.Instance.PlayClick();
        }
    }

    void OnRelease()
    {
        normal.SetActive(true);
        pressed.SetActive(false);
    }

    void OnClick()
    {
        SceneManager.LoadScene((int)scene);
    }
}
