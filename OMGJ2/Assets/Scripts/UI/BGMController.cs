﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BGMController: MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip menuMusic;
    public AudioClip[] levelMusic;

    private Scene currentScene;

    public void Start()
    {
        DontDestroyOnLoad(this.gameObject);

        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    private void OnLevelFinishedLoading(UnityEngine.SceneManagement.Scene scene, LoadSceneMode mode)
    {
        Scene newScene = (Scene)scene.buildIndex;
        switch (newScene)
        {
            case Scene.TitleScreen:
                break;
            case Scene.Gameplay:
                // play gameplay music
                if (audioSource.isPlaying)
                {
                    audioSource.Stop();
                }
                audioSource.clip = GetClip(currentScene);
                if (audioSource.clip != null)
                {
                    audioSource.Play();
                }
                break;
            default:
                if (audioSource.clip != menuMusic)
                {
                    if (audioSource.isPlaying)
                    {
                        audioSource.Stop();
                    }
                    audioSource.volume = 0.8f;
                    audioSource.clip = menuMusic;
                    audioSource.Play();
                }
                break;
        }

        currentScene = newScene;

    }

    private AudioClip GetClip(Scene scene)
    {
        AudioClip clip = null;
        switch (scene)
        {
            case Scene.World0:
                clip = levelMusic[0];
                audioSource.volume = 0.6f;
                break;
            case Scene.World1:
                clip = levelMusic[1];
                audioSource.volume = 0.6f;
                break;
            case Scene.World2:
                clip = levelMusic[2];
                audioSource.volume = 0.5f;
                break;
            case Scene.World3:
                clip = levelMusic[3];
                audioSource.volume = 0.5f;
                break;
            case Scene.World4:
                clip = levelMusic[4];
                audioSource.volume = 0.8f;
                break;
        }

        return clip;
    }
}
