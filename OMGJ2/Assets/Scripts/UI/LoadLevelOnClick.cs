﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevelOnClick : MonoBehaviour
{
    public TextAsset levelFile;
    public RoundDef[] rounds;
    public int passingScore;
    public GameObject normal;
    public GameObject pressed;
    public GameObject star;
    public GameObject sparklyStar;
    public GameObject lockIcon;
    public bool won;

    enum State
    {
        Locked,
        Normal,
        Pressed
    }

    private State state;

    public void Init(bool prevLevelCompleted)
    {
        normal.SetActive(true);
        pressed.SetActive(false);

        string wonKey = levelFile.name + "_won";
        string masteredKey = levelFile.name + "_mastered";

        won = prevLevelCompleted && PlayerPrefs.HasKey(wonKey) && PlayerPrefs.GetInt(wonKey) == 1;
        bool mastered = PlayerPrefs.HasKey(masteredKey) && PlayerPrefs.GetInt(masteredKey) == 1;

        state = prevLevelCompleted ? State.Normal : State.Locked;

        switch (state)
        {
            case State.Locked:
                lockIcon.SetActive(true);
                break;
            case State.Normal:
                lockIcon.SetActive(false);
                break;
        }

        if (won)
        {
            if (mastered)
            {
                star.SetActive(false);
                sparklyStar.SetActive(true);
            }
            else
            {
                star.SetActive(true);
                sparklyStar.SetActive(false);
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case State.Pressed:
                if (Input.GetMouseButtonUp(0))
                {
                    Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero);
                    if (hit && hit.collider != null)
                    {
                        if (hit.collider.gameObject == this.gameObject)
                        {
                            OnClick();
                        }
                    }

                    OnRelease();
                }
                break;
        }
    }

    void OnMouseDown()
    {
        if (state != State.Locked)
        {
            OnPress();
        }
    }

    void OnPress()
    {
        if (state != State.Pressed)
        {
            normal.SetActive(false);
            pressed.SetActive(true);
            state = State.Pressed;
            SFXController.Instance.PlayClick();
        }
    }

    void OnRelease()
    {
        normal.SetActive(true);
        pressed.SetActive(false);
        state = State.Normal;
    }

    void OnClick()
    {
        GameplaySettings.Level = levelFile;
        GameplaySettings.Rounds = rounds;
        GameplaySettings.PassingScore = passingScore;
        SceneManager.LoadScene((int)Scene.Gameplay);
    }
}