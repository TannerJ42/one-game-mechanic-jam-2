﻿using System.Collections.Generic;
using UnityEngine;

public class SFXController : MonoBehaviour
{
    public List<AudioSource> audioSources;
    public AudioClip[] clips;

    public static SFXController Instance;

    void Awake()
    {
        DontDestroyOnLoad(this);

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            DestroyObject(gameObject);
        }
    }

    public void PlayClick()
    {
        if (audioSources == null)
        {
            return;
        }
        AudioSource source = null;
        for (int i = 0; i < audioSources.Count; i++)
        {
            if (!audioSources[i].isPlaying)
            {
                source = audioSources[i];
            }
        }

        if (source == null) 
        {
            if (audioSources.Count < 10)
            {
                source = this.gameObject.AddComponent<AudioSource>();
                audioSources.Add(source);
            }
            else
            {
                source = audioSources[0];
                source.Stop();
            }
        }
        source.volume = 0.8f;
        source.clip = clips[Random.Range(0, clips.Length)];
        source.Play();
    }   
}
