﻿using UnityEngine;

public class KeyUIController : MonoBehaviour
{
    public SpriteRenderer first;
    public SpriteRenderer second;
    public SpriteRenderer third;
    public SpriteRenderer fourth;
    public SpriteRenderer fifth;
    public SpriteRenderer sixth;

    public SpriteRenderer arrow1;
    public SpriteRenderer arrow2;
    public SpriteRenderer arrow3;
    public SpriteRenderer arrow4;
    public SpriteRenderer arrow5;

    public void SetKey(TileType[] order, Sprite[] sprites)
    {
        first.sprite = sprites[(int)order[0] - 1];
        if (order.Length > 1)
            second.sprite = sprites[(int)order[1] - 1];
        if (order.Length > 2)
            third.sprite = sprites[(int)order[2] - 1];
        if (order.Length > 3)
            fourth.sprite = sprites[(int)order[3] - 1];
        if (order.Length > 4)
            fifth.sprite = sprites[(int)order[4] - 1];

        switch (order.Length)
        {
            case 1:
                third.enabled = false;
                fourth.enabled = false;
                fifth.enabled = false;
                sixth.enabled = false;
                arrow1.enabled = true;
                arrow2.enabled = false;
                arrow3.enabled = false;
                arrow4.enabled = false;
                arrow5.enabled = false;
                second.sprite = sprites[(int)order[0] - 1];
                break;
            case 2:
                third.enabled = true;
                fourth.enabled = false;
                fifth.enabled = false;
                sixth.enabled = false;
                arrow1.enabled = true;
                arrow2.enabled = true;
                arrow3.enabled = false;
                arrow4.enabled = false;
                arrow5.enabled = false;
                third.sprite = sprites[(int)order[0] - 1];
                break;
            case 3:
                third.enabled = true;
                fourth.enabled = true;
                fifth.enabled = false;
                sixth.enabled = false;
                arrow1.enabled = true;
                arrow2.enabled = true;
                arrow3.enabled = true;
                arrow4.enabled = false;
                arrow5.enabled = false;
                fourth.sprite = sprites[(int)order[0] - 1];
                break;
            case 4:
                third.enabled = true;
                fourth.enabled = true;
                fifth.enabled = true;
                sixth.enabled = false;
                arrow1.enabled = true;
                arrow2.enabled = true;
                arrow3.enabled = true;
                arrow4.enabled = true;
                arrow5.enabled = false;
                fifth.sprite = sprites[(int)order[0] - 1];
                break;
            case 5:
                third.enabled = true;
                fourth.enabled = true;
                fifth.enabled = true;
                sixth.enabled = true;
                arrow1.enabled = true;
                arrow2.enabled = true;
                arrow3.enabled = true;
                arrow4.enabled = true;
                arrow5.enabled = true;
                sixth.sprite = sprites[(int)order[0] - 1];
                break;
        }
    }
}

