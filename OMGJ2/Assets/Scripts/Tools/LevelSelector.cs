﻿#if UNITY_EDITOR
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

public class LevelSelector : EditorWindow
{
    private int selectedIndex;

    [MenuItem("Tools/Level Editor", priority = 10)]
    public static void Init()
    {
        UnityEditor.EditorApplication.isPlaying = false;
        LevelSelector window = (LevelSelector)EditorWindow.GetWindow(typeof(LevelSelector));
        window.position = new Rect(200, 200, 400, 200);
        window.Show();
    }

    public void OnGUI()
    {
        GUILayout.Label("Select or Create a Level", EditorStyles.boldLabel);

        List<string> levelNames = new List<string>();
        var levelObjects = Resources.LoadAll("Levels", typeof(TextAsset));

        foreach (Object obj in levelObjects)
        {
            TextAsset text = obj as TextAsset;
            if (text != null)
            {
                levelNames.Add(text.name);
            }
        }

        selectedIndex = EditorGUILayout.Popup("Level", selectedIndex, levelNames.ToArray());
        EditorGUI.BeginDisabledGroup(selectedIndex < 0 || selectedIndex >= levelNames.Count);
        if (GUILayout.Button("Edit Level", GUILayout.Width(200)))
        {
            UnityEditor.EditorApplication.isPlaying = false;
            EditorSceneManager.OpenScene("Assets/TileEditor/LevelEditor.unity");
            UnityEditor.EditorApplication.isPlaying = true;
            LevelDef def = JsonUtility.FromJson<LevelDef>((levelObjects[selectedIndex] as TextAsset).text);
            LevelEditor.Load(def);
            Close();
        }
        if (GUILayout.Button("Play Level", GUILayout.Width(200)))
        {
            GameplaySettings.Level = levelObjects[selectedIndex] as TextAsset;

            EditorSceneManager.OpenScene("Assets/Resources/Scenes/Gameplay.unity");

            UnityEditor.EditorApplication.isPlaying = true;
        }

        EditorGUI.EndDisabledGroup();
        if (GUILayout.Button("Create New Level", GUILayout.Width(200)))
        {
            UnityEditor.EditorApplication.isPlaying = false;
            EditorSceneManager.OpenScene("Assets/TileEditor/LevelEditor.unity");
            UnityEditor.EditorApplication.isPlaying = true;

            LevelCreator.Init();
            Close();
        }
    }
}
#endif