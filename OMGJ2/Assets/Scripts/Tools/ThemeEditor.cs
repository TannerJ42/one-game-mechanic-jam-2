﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ThemeEditor : EditorWindow
{

    [MenuItem("Tools/Theme Editor", priority = 20)]
    static void Init()
    {
        ThemeEditor window = (ThemeEditor)EditorWindow.GetWindow(typeof(ThemeEditor));
        window.Show();
    }

    public void OnGUI()
    {
        GUILayout.Label("Create a theme.", EditorStyles.boldLabel);
    }
}
#endif