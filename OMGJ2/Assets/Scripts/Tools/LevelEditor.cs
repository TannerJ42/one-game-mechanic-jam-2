﻿#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

[ExecuteInEditMode]
public class LevelEditor : EditorWindow
{
    private LevelDef originalDef;
    private LevelDef newDef;



    public static void Load(LevelDef def)
    {
        LevelEditor editor = ScriptableObject.CreateInstance<LevelEditor>();
        editor.SetLevelDef(def);
        editor.StartEditing();
        editor.Show();
    }

    public void StartEditing()
    {
        TileManager manager = GameObject.Find("SceneEditor").GetComponent<TileManager>();
        manager.setDef(newDef);
    }

    public void SetLevelDef(LevelDef def)
    {
        this.originalDef = def;
        this.newDef = new LevelDef();
        newDef.name = originalDef.name;
        newDef.width = originalDef.width;
        newDef.height = originalDef.height;
        newDef.theme = originalDef.theme;
        newDef.tiles = originalDef.tiles;
    }

    public void OnGUI()
    {
        GUILayout.Label("Level Editor", EditorStyles.boldLabel);
        EditorGUILayout.LabelField("Level name: " + newDef.name);
        newDef.theme = (Theme)EditorGUILayout.EnumPopup("Theme:", newDef.theme);

        if (GUILayout.Button("Save", GUILayout.Width(200)))
        {
            Save();
        }
        if (GUILayout.Button("Delete", GUILayout.Width(200)))
        {
            if (EditorUtility.DisplayDialog("Delete level?", "This is permanent.", "Yes", "Fuck no"))
            {
                Delete();
                LevelSelector.Init();
                Close();
            }
        }
        if (GUILayout.Button("Back", GUILayout.Width(200)))
        {
            LevelSelector.Init();
            Close();
        }
    }

    void Save()
    {
        TileManager manager = GameObject.Find("SceneEditor").GetComponent<TileManager>();
        LevelEditorTileController[] controllers = manager.GetComponentsInChildren<LevelEditorTileController>();
        this.newDef.tiles = new List<TileDef>();
        for (int i = 0; i < controllers.Length; i++)
        {
            TileDef contDef = controllers[i].getDef();
            TileDef newTileDef = new TileDef();
            newTileDef.type = contDef.type;
            newTileDef.x = contDef.x;
            newTileDef.y = contDef.y;
            this.newDef.tiles.Add(newTileDef);
        }

        string json = JsonUtility.ToJson(newDef);

        string path = "Assets/Resources/Levels/" + newDef.name + ".txt";
        using (FileStream fs = new FileStream(path, FileMode.Create))
        {
            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.Write(json);
            }
        }

        UnityEditor.AssetDatabase.Refresh();
    }

    void Delete()
    {
        string path = "Assets/Resources/Levels/" + newDef.name + ".txt";
        AssetDatabase.DeleteAsset(path);
        UnityEditor.AssetDatabase.Refresh();
    }

}
#endif
