﻿#if UNITY_EDITOR
using System.Collections.Generic;

using UnityEditor;
using UnityEngine;

class LevelCreator : EditorWindow
{

    private LevelDef def;

    public static void Init()
    {
        LevelCreator window = new LevelCreator();
        window.Show();
    }

    public void Awake()
    {
        def = new LevelDef();
        def.name = "";
        def.theme = Theme.None;
        def.width = 8;
        def.height = 12;
        def.tiles = new List<TileDef>();

    }

    public void OnGUI()
    {
        def.name = EditorGUILayout.TextField("Name:", def.name);
        def.theme = (Theme)EditorGUILayout.EnumPopup("Theme", def.theme);
        def.width = EditorGUILayout.IntField("Columns", def.width);
        def.height = EditorGUILayout.IntField("Rows", def.height);

        EditorGUI.BeginDisabledGroup(def.name == null || def.name == "");
        if (GUILayout.Button("Next", GUILayout.Width(200)))
        {
            Edit();
        }
        if (GUILayout.Button("Cancel", GUILayout.Width(200)))
        {
            Back();
        }
        EditorGUI.EndDisabledGroup();
    }

    private void Edit()
    {
        for (int x = 0; x < def.width; x++)
        {
            for (int y = 0; y < def.height; y++)
            {
                TileDef tileDef = new TileDef();
                tileDef.x = x;
                tileDef.y = y;
                tileDef.type = TileType.One;
                def.tiles.Add(tileDef);
            }
        }

        LevelEditor.Load(def);
        Close();
    }

    private void Back()
    {
        LevelSelector.Init();
        Close();
    }

}
#endif