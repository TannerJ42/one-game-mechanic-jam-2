﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WorldController : MonoBehaviour {

    public LoadLevelOnClick[] levels;
    public Theme theme;
    public Sprite[] sprites;
    public Sprite background;

	// Use this for initialization
	void Start () {
        GameplaySettings.Theme = theme;
        GameplaySettings.Sprites = sprites;
        GameplaySettings.background = background;
        bool prevLevelCompleted = true;
		for (int i = 0; i < levels.Length; i++)
        {
            LoadLevelOnClick level = levels[i];
            level.Init(prevLevelCompleted);
            prevLevelCompleted = level.won;
        }

        GameplaySettings.LastWorldScene = (Scene)SceneManager.GetActiveScene().buildIndex;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
